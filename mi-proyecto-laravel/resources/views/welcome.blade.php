<!doctype html>
<html lang="es">
    <head>
        <style>

            label{
                color:black;
                font-weight: bold;
            }
            {
              font-size: 18px;
            }
            #labelNumero{
                font-size: 28px;
            }
            .color_azul{
                color: cadetblue;
            }
            @keyframes prueba {
                0%{
                    left: 0px;
                }
                45%{
                    left: 100px;
                }
                100%{
                    left: 400px ;
                    background-color: cornflowerblue;
                }
            }
            label{
                /* Animacion*/
                animation-name: prueba;
                animation-duration: 2s;
                animation-iteration-count: infinite;
                animation-direction: alternate;/* repetir la animacion */
                /*animacion*/
                width: fit-content;
                background-color: mistyrose;
                position: relative;
                padding: 10px;
            }

            #container{
                background-color: #bce8f1;
                width: 500px;
                height: 400px;
                position: relative;
            }
            @-webkit-keyframes nieve{
                from {top: -10px;}
                to {top: 550px;}
            }

            @-webkit-keyframes copos{
                0% { -webkit-transform: rotate(-180deg) translate(0px, 0px);}
                50% { -webkit-transform: rotate(180deg) translate(50px, 80px);}
                100% { -webkit-transform: rotate(180deg) translate(100px, 175px);}
            }

            #snow div {
                position: absolute;
                top: -40px;
                -webkit-animation-name: nieve, copos;
                -webkit-animation-iteration-count: infinite;
                -webkit-animation-direction: normal;
                -webkit-animation-timing-function: ease-in;
            }
            .copos {
                color: #FFF;
                font-size: 3em;
                position: absolute;
            }
            .copos.f1 {
                left: 140px;
                -webkit-animation-duration: 5s;
            }
            .copos.f2 {
                font-size: 3em;
                left: 220px;
                -webkit-animation-duration: 7s;
            }
            .copos.f3 {
                font-size: 3em;
                left: 300px;
                -webkit-animation-duration: 8s;
            }
            .copos.f4 {
                font-size: 3em;
                left: 480px;
                -webkit-animation-duration: 10s;
            }
            .copos.f5 {
                font-zise: 3em;
                left: 520px;
                -wedkit-animation-duration: 8s;
            }
        </style>
    </head>
    <body>

    <label>Datos del alumno:</label>
    <br><br>
    <br>
    <label>Nombre(s):</label>
    <input type="text"name="text" id="text" class="label">
    <br>
    <br>
    <label>Apellido Paterno:</label>
    <input type="text"name="texto">
    <br>
    <br>
    <label>Apellido materno:</label>
    <input type="text"name="texto">
    <br>
    <br>
    <label>Edad:</label>
    <input type="number"name="numero" id="numero">
    <br>
    <br>
    <label>Genero:</label>
    <br>
    <br>
    <label><input type="radio" name="genero" value="M">Masculino</label>
    <label><input type="radio" name="genero" value="F">Femenino</label>
    <br>
    <br>
    <label>Alergias:</label>
     <select name="texto">
      <option value="asma">asma</option>
      <option value="farmacos">farmacos</option>
      <option value="alergenicos">alergenicos</option>
      <option value="conjuntivitis">conjuntivitis</option>
      <option value="ninguna">ninguna</option>
    </select>
    <br>
    <br>
    <label>comentario:</label>
    <textarea name="comentario" id="comentario"></textarea>
    <br>
    <br>
    <label>Datos del tutor:</label>
    <br><br>
    <br>
    <label>Nombre completo:</label>
    <input type="text"name="texto" id="texto">
    <br>
    <br>
    <label>Parentesco:</label>
    <input type="text"name="texto" id="texto">
    <br>
    <br>
    <label>Direccion:</label>
    <input type="text"name="texto" id="texto">
    <br>
    <br>
    <label>Estado:</label>
    <select name="texto" id="texto">
    <option value="oaxaca">oaxaca</option>
    <option value="puebla">puebla</option>
    <option value="tabasco">tabasco</option>
    <option value="tlaxcala">tlaxcala</option>
    <option value="veracruz">veracruz</option>
    </select>
    <br>
    <br>
    <label>Ciudad:</label>
    <input type="text"name="texto" id="texto">
    <br>
    <br>
    <label>C.P:</label>
    <input type="number"name="numero" id="numero">
    <br>
    <br>
    <label>Numero Int:</label>
    <input type="number"name="numero" id="numero">
    <br>
    <br>
    <label>Numero Ext:</label>
    <input type="number"name="numero" id="numero">
    <br>
    <br>
    <label>Domicilio:</label>
    <input type="text"name="texto" id="texto">
    <br>
    <br>
    Datos Socioeconomicos:
    <br>
    <br>
    <label>Numero de cuartos:</label>
    <br>
    <br>
    <label><input type="radio" name="number" value="numero">1-3</label>
    <label><input type="radio" name="number" value="numero">3-5</label>
    <label><input type="radio" name="number" value="numero">5 o mas</label>
    <br>
    <br>
    <label>Cuenta con:</label>
    <br>
    <br>
    <label><input type="radio" name="text" value="texto">televisor</label>
    <label><input type="radio" name="text" value="texto">lavadora</label>
    <label><input type="radio" name="text" value="texto">estufa</label>
    <label><input type="radio" name="text" value="texto">secadora</label>
    <label><input type="radio" name="text" value="texto">refrigerador</label>
    <br>
    <br>
    <label>Con cuantos baños cuenta la vivienda:</label>
    <br>
    <br>
    <label><input type="radio" name="number" value="numero">1</label>
    <label><input type="radio" name="number" value="numero">2</label>
    <label><input type="radio" name="number" value="numero">3 o mas</label>

    <body id="container">
    <div>

        <div id="snow" class="snow">
            <div class="copos f1">*</div>
            <div class="copos f2">*</div>
            <div class="copos f3">*</div>
            <div class="copos f4">*</div>
            <div class="copos f5">*</div>
        </div>
        <div id="ground">
        </div>

    </div>
    </body>

</html>

